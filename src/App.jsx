import { useEffect, useState } from 'react';
import './App.css';

function App() {
  const [time, settime] = useState({hr:0,m:0,s:0,ms:0})
  const [lap, setlap] = useState([])
  const [play, setplay] = useState(false)

  function z(e){
    return e<10? '0'+e : e;
  }
  function timer(){
    let {hr,m,s,ms}=time;
    if(time.ms===90){
      settime({...time,s:time.s+1,ms:0})
    }
    if(time.s===60){
      settime({...time,m:time.m+1,s:0})
    }
    if(time.m===60){
      settime({...time,hr:time.hr+1,m:0})
    }
    

  }
  const ret= ()=>{
    setlap([]);
    setplay(false);
    settime({hr:0,m:0,s:0,ms:0});
}
function flap(){
  setlap([time,...lap])
}

 useEffect(() => {
   const interval = play&&setInterval(() => {
    settime({...time,ms:time.ms+1})
     timer()
    }, 10);


   return () => {
     clearInterval(interval);
   };
 }, [time,play]);
 

  return (
    <div className="App">

      <div className="timer">
        <h2>{`${z(time.hr)}:${z(time.m)}:${z(time.s)}:${z(time.ms)}`}</h2>
        <div className="btn">
          <button onClick={() => setplay(!play)}>start/stop</button>
          <button onClick={() => ret()}>end</button>
         {play&& <button onClick={() => flap()}>lap</button>}
        </div>

      </div>
      <div className="laps">
        {lap.map(({hr,m,s,ms},i)=>
        <li key={i} >#{i} {`${z(hr)}:${z(m)}:${z(s)}:${z(ms)}`}</li>
          )}
      </div>
      
    </div>
  );
}

export default App;
